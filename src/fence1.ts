import * as THREE from 'three'
import { Scene, PerspectiveCamera, WebGLRenderer } from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { Line2 } from 'three/examples/jsm/lines/Line2.js'
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial.js'
import Stats from 'stats.js'
import FenceClass, { pointsTypeXZ } from './fenceClass'
import { getLine2, simpleLineSegments } from './mesh1'

let stats: any
const clock = new THREE.Clock()
const maxSpeed = 80
let speed
let capacity
const sizes: { width: number; height: number } = {
  width: window.innerWidth,
  height: window.innerHeight,
}
const cursor: { x: number; y: number } = {
  x: 0,
  y: 0,
}
let scene: Scene, camera: PerspectiveCamera, renderer: WebGLRenderer

const fences: FenceClass[] = []
const fencesLine: Line2[] = []

export type lineType = { [key: string]: LineMaterial }

const lineMaterials: lineType = {
  line1: new LineMaterial({
    color: 0x08641e,
    linewidth: 0.005,
    dashed: false,
  }),
  line2: new LineMaterial({
    color: 0x721e1e,
    linewidth: 0.005,
    dashed: false,
  }),
  line3: new LineMaterial({
    color: 0xf14646,
    linewidth: 0.005,
    dashed: false,
  }),
  line4: new LineMaterial({
    color: 0xf5b719,
    linewidth: 0.005,
    dashed: false,
  }),
  line5: new LineMaterial({
    color: 0xfee400,
    linewidth: 0.005,
    dashed: false,
  }),
  line6: new LineMaterial({
    color: 0x2cf65c,
    linewidth: 0.005,
    dashed: false,
  }),
}

function init() {
  const canvasEl: Element | null = document.querySelector('.canvas')
  if (!canvasEl) {
    return
  }
  const canvas = canvasEl as HTMLElement

  renderer = new THREE.WebGLRenderer({ canvas, antialias: true })
  renderer.setSize(window.innerWidth, window.innerHeight)
  renderer.setClearColor(0x000000)

  scene = new THREE.Scene()

  camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height)
  camera.position.set(0, 10, 15)
  camera.lookAt(scene.position)

  const controls = new OrbitControls(camera, canvas)
  // controls.enableDamping = true

  const light = new THREE.AmbientLight(0xefefef, 1.5)
  const pointLight = new THREE.PointLight(0xff9000, 3)
  pointLight.position.set(3, 3, 3)
  const pointLight2 = new THREE.PointLight(0x000000, 6)
  pointLight2.position.set(-3, -3, 3)

  scene.add(light)
  scene.add(pointLight)
  scene.add(pointLight2)

  const axesHelper = new THREE.AxesHelper(50)
  scene.add(axesHelper)
  const gridHelper = new THREE.GridHelper(100, 100)
  // gridHelper.rotation.x = -Math.PI / 2
  scene.add(gridHelper)

  const isMaterialInPx = true

  stats = new Stats()
  document.body.appendChild(stats.dom)

  const geometry = new THREE.PlaneGeometry(6, 6)
  const texture = new THREE.TextureLoader().load('jpg/wood1.jpg')
  const material = new THREE.MeshPhongMaterial({ color: 0xffffff, map: texture })
  const plane = new THREE.Mesh(geometry, material)
    // scene.add(plane)


    /*** СОЗДАЕМ ЗАБОРЫ ************************************************************ */

  const pointsXZ: pointsTypeXZ[] = [
    { x: 0, z: 0 },
    { x: -0.0, z: -10 },
    { x: 10, z: -10 },
  ]
  createFenceSegmentMain({ pointsXZ, y: 1, fences, fencesLine, roadId: 1, speedMax: 50, capacityMax: 40 })

  const pointsXZ_2: pointsTypeXZ[] = [
    { x: 10, z: 0 },
    { x: 12, z: -2 },
    { x: 17, z: -2 },
    { x: 19, z: -4 },
  ]
  createFenceSegmentMain({
    pointsXZ: pointsXZ_2,
    y: 1.2,
    fences,
    fencesLine,
    roadId: 2,
    speedMax: 150,
    capacityMax: 140,
  })
  const pointsXZ_3: pointsTypeXZ[] = [
    { x: -0, z: 10 },
    { x: -12, z: 12 },
    { x: -7, z: 12 },
    { x: -9, z: 14 },
  ]
  createFenceSegmentMain({ pointsXZ: pointsXZ_3, y: 1, fences, fencesLine, roadId: 3, speedMax: 30, capacityMax: 410 })

  const pointsXZ_4: pointsTypeXZ[] = [
    { x: -20, z: -20 },
    { x: 20, z: -20 },
  ]
  createFenceSegmentMain({ pointsXZ: pointsXZ_4, y: 2, fences, fencesLine, roadId: 4, speedMax: 100, capacityMax: 100 })


  /*** ТРЕУГОЛЬНИК ******************************************************* * /
  // Создаем вершины треугольника
  // prettier-ignore
  var vertices = [
    -20, 0, -29,  // вершина 1
    20, 0, -11,  // вершина 2
    20, 0, -1  // вершина 3
  ]
  // prettier-ignore
  const vertices11 = [
    -20, 1.2, -29, 
    20, 1.2, -11, 
    20, 1.2, -11]
  // Создаем грани для треугольника
  var indices = new Uint16Array([0, 1, 2])

  // Создаем буферную геометрию
  var geometryBuf = new THREE.BufferGeometry()
  geometryBuf.setAttribute('position', new THREE.BufferAttribute(new Float32Array(vertices), 3)) // 3 - количество координат для каждой вершины
  geometryBuf.setIndex(new THREE.BufferAttribute(indices, 1)) // 1 - количество индексов для каждой грани

  var materialBuf = new THREE.MeshBasicMaterial({ color: 0xffff00, side: THREE.DoubleSide, map: texture })

  var triangleBuf = new THREE.Mesh(geometryBuf, materialBuf)

  triangleBuf.rotation.x = -Math.PI / 2

  // Добавляем треугольник на сцену
  scene.add(triangleBuf)
  
  /*** КАРТИНА ******************************************************* */

  // var quad_vertices = [-30.0, 30.0, 0.0, 30.0, 30.0, 0.0, 30.0, -30.0, 0.0, -30.0, -30.0, 0.0]
  var quad_vertices = [-10.0, 10.0, 0.0, 10.0, 10.0, 0.0, 10.0, -10.0, 0.0, -10.0, -10.0, 0.0]
  // var quad_vertices = [-60.0, 30.0, 0.0, 70.0, 30.0, 0.0, 70.0, -30.0, 0.0, -60.0, -30.0, 0.0]

  // var quad_uvs = [0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0] // перевернута!
  // var quad_uvs = [0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0] // на боку
     var quad_uvs = [0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0] // норм!

  var quad_indices = [0, 2, 1, 0, 3, 2]
  // var quad_indices = [1, 0, 2, 0, 3, 2]

  var geometryBufText = new THREE.BufferGeometry()

  var verticesBufText = new Float32Array(quad_vertices)
  // Each vertex has one uv coordinate for texture mapping
  var uvs = new Float32Array(quad_uvs)
  // Use the four verticesBufText to draw the two triangles that make up the square.
  var indicesBufText = new Uint32Array(quad_indices)

  // itemSize = 3 because there are 3 values (components) per vertex
  geometryBufText.setAttribute('position', new THREE.BufferAttribute(verticesBufText, 3))
  geometryBufText.setAttribute('uv', new THREE.BufferAttribute(uvs, 2))
  geometryBufText.setIndex(new THREE.BufferAttribute(indicesBufText, 1))

  const sprite = new THREE.TextureLoader().load('pics/vesna.jpg') // kot

  sprite.magFilter = THREE.LinearFilter
  // sprite.magFilter = THREE.NearestFilter
  sprite.colorSpace = THREE.SRGBColorSpace
  sprite.generateMipmaps = true
  sprite.minFilter = THREE.LinearMipmapLinearFilter
  // sprite.repeat.set(4, 4)

  var materialBufText = new THREE.MeshStandardMaterial({ map: sprite })
  var meshBufText = new THREE.Mesh(geometryBufText, materialBufText)
  // meshBufText.position.z = -100;

  scene.add(meshBufText)

  /********************************************************** */

  getLine2({ scene })

  // simpleLineSegments({ scene })
  // getLine2_111({ scene })

  /********************************************************** */
  const sdvig = 45
  const qty = 10


  for (let i = 0; i < qty; i++) {
    //console.log('i=', i)
    const pointsXZ_i: pointsTypeXZ[] = [
      { x: -20 + sdvig, z: -20 - i },
      { x: 20 + sdvig, z: -20 - i },
    ]
    createFenceSegmentMain({ pointsXZ: pointsXZ_i, y: 2, fences, fencesLine, roadId: i, speedMax: 100, capacityMax: 100 })
  }

  function animate() {
    checkCameraAngle(camera)
    getData()
    stats && stats.update()

    renderer.render(scene, camera)
  }

  renderer.setAnimationLoop(animate)
}

init()

function createFenceSegmentMain({
  pointsXZ,
  y,
  fences,
  fencesLine,
  roadId,
  speedMax = 80,
  capacityMax = 80,
}: {
  pointsXZ: pointsTypeXZ[]
  y: number
  fences: FenceClass[]
  fencesLine: Line2[]
  roadId: number
  speedMax: number
  capacityMax: number
}) {
  const fenceClassSegment1 = new FenceClass({ position: [], roadId, speedMax, capacityMax })

  console.log('fenceClassSegment1 = ', fenceClassSegment1)
  
  const dataFence = fenceClassSegment1.createFenceByPointsXZ({ pointsXZ, y: 1.2 })
  if (dataFence?.meshFence) {
    scene.add(dataFence.meshFence)
    fences.push(fenceClassSegment1)
  }

  if (dataFence?.meshFenceHead) {
    scene.add(dataFence.meshFenceHead)
    fences.push(fenceClassSegment1)
    return dataFence.meshFenceHead
  } else {
    return null
  }
}

function getData() {
  const t = clock.getElapsedTime() * 1 //3
  const speed = ((Math.sin(t) + 1) / 2) * maxSpeed

  for (const fence in fences) {
    fences[fence].setHeightByCapacity(speed)
    ///
    // fences[fence].setColorBySpeed(speed, lineMaterials)
  }
}

window.addEventListener('resize', () => {
  sizes.width = window.innerWidth
  sizes.height = window.innerHeight

  camera.aspect = sizes.width / sizes.height
  camera.updateProjectionMatrix()

  renderer.setSize(sizes.width, sizes.height)
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
  renderer.render(scene, camera)
})

function checkCameraAngle(camera: PerspectiveCamera) {
  let cameraDirection = new THREE.Vector3()
  camera.getWorldDirection(cameraDirection)

  let horizontalPlaneDirection = new THREE.Vector3(cameraDirection.x, 0, cameraDirection.z).normalize() // Нормализуем вектор, чтобы получить направление на горизонтальную плоскость xz
  let angle = Math.acos(cameraDirection.dot(horizontalPlaneDirection)) * (180 / Math.PI) // Вычисляем угол между направлением камеры и горизонтальной плоскостью xz

  if (angle >= 60 && angle <= 120) {
    for (const line in fencesLine) {
      fencesLine[line].visible = true
    }
  } else {
    for (const line in fencesLine) {
      fencesLine[line].visible = false
    }
  }
}
