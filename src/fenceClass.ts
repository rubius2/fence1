import * as THREE from 'three'
import { Mesh, Points, MeshPhongMaterial, ShaderMaterial } from 'three'

import { Line2 } from 'three/examples/jsm/lines/Line2.js'
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial.js'
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry.js'

import * as GeometryUtils from 'three/examples/jsm/utils/GeometryUtils.js'
import { lineType } from './fence1'

type fenceProps = {
  roadId: number
  position: number[]
  speedMax: number
  capacityMax: number
}

export type pointsTypeXZ = {
  x: number
  z: number
}

type fenceParamsPos = {
  position: number[]
  height: number
  color: number
  material: MeshPhongMaterial | ShaderMaterial
}
type fenceParams = {
  segmentPositions: number[]
  height: number
  color: number
  material: MeshPhongMaterial | ShaderMaterial
}

export default class FenceClass {
  protected roadId: number
  protected position: number[]
  protected speedMax: number
  protected capacityMax: number

  protected isCreateFence: boolean

  protected speed: number
  protected height: number
  protected heightMin: number
  protected heightMax: number
  protected color: number
  protected capacity: number
  protected meshFence: THREE.Mesh | null
  protected meshFenceHead: THREE.Mesh | null
  protected meshLine: THREE.Mesh | null
  protected segmentPositions: number[]

  constructor({ roadId, position, speedMax, capacityMax }: fenceProps) {
    this.roadId = roadId
    this.position = position
    this.speedMax = speedMax
    this.capacityMax = capacityMax

    this.speed = 50

    this.height = 1
    this.color = 0x00ff00
    this.capacity = 5
    this.heightMin = 0.1
    this.heightMax = 2
    this.meshFence = null
    this.meshFenceHead = null
    this.meshLine = null
    this.segmentPositions = []
    this.isCreateFence = false // временно создал, чтобы понять каким образом создается объект
  }

  set setCapacityMax(val: number) {
    if (val > 0) {
      this.capacityMax = val
    }
  }
  set setSpeedMax(val: number) {
    if (val > 0) {
      this.speedMax = val
    }
  }

  drawPlainVertices({
    point1,
    point2,
    vertices,
  }: {
    point1: THREE.Vector3
    point2: THREE.Vector3
    vertices: number[]
  }) {
    // triangle 1
    vertices.push(point1.x, point1.y, point1.z)
    vertices.push(point2.x, point1.y, point2.z)
    vertices.push(point2.x, point2.y, point2.z)

    // triangle 2
    vertices.push(point1.x, point1.y, point1.z)
    vertices.push(point2.x, point2.y, point2.z)
    vertices.push(point1.x, point2.y, point1.z)
  }
  drawPlainVerticesEdge1({
    point1,
    point2,
    point3,
    point4,
    verticesHead,
  }: {
    point1: THREE.Vector3
    point2: THREE.Vector3
    point3: THREE.Vector3
    point4: THREE.Vector3
    verticesHead: number[]
  }) {
    // triangle 1
    verticesHead.push(point1.x, point1.y, point1.z)
    verticesHead.push(point3.x, point3.y, point3.z)
    verticesHead.push(point2.x, point2.y, point2.z)

    // triangle 2
    verticesHead.push(point1.x, point1.y, point1.z)
    verticesHead.push(point2.x, point2.y, point2.z)
    verticesHead.push(point4.x, point4.y, point4.z)
  }

  getIntersectionPoint(v1: THREE.Vector3, v2: THREE.Vector3) {
    let intersectionPoint

    // Находим направления прямых, проходящих через начальные точки векторов
    const direction1 = v1.clone().normalize()
    const direction2 = v2.clone().normalize()

    // Находим точку пересечения прямых
    const origin1 = v1.clone()
    const origin2 = v2.clone()
    const normalVector = new THREE.Vector3()
    normalVector.crossVectors(direction1, direction2)

    // Если векторы не параллельны
    if (normalVector.lengthSq() > Number.EPSILON) {
      const diff1 = origin2.clone().sub(origin1)
      const d = direction1.clone().cross(direction2)
      const t1 = diff1.dot(d) / normalVector.lengthSq()

      // Найденная точка пересечения
      intersectionPoint = origin1.clone().add(direction1.clone().multiplyScalar(t1))
    } else {
      // В случае параллельных векторов
      intersectionPoint = null
    }
    return intersectionPoint
    // intersectionPoint содержит точку пересечения двух векторов или null, если векторы параллельны
  }
  findIntersectionPoint({
    p1x1,
    p1z1,
    p1x2,
    p1z2,
    p2x1,
    p2z1,
    p2x2,
    p2z2,
  }: {
    p1x1: number
    p1z1: number
    p1x2: number
    p1z2: number
    p2x1: number
    p2z1: number
    p2x2: number
    p2z2: number
  }) {
    // Уравнение прямой 1: y = m1*x + b1
    const m1 = (p1z2 - p1z1) / (p1x2 - p1x1)
    const b1 = p1z1 - m1 * p1x1

    // Уравнение прямой 2: y = m2*x + b2
    const m2 = (p2z2 - p2z1) / (p2x2 - p2x1)
    const b2 = p2z1 - m2 * p2x1

    // X-координата точки пересечения
    const intersectionX = (b2 - b1) / (m1 - m2)

    // Y-координата точки пересечения
    const intersectionY = m1 * intersectionX + b1

    return { x: intersectionX, z: intersectionY }
  }

  findPerpendic(point2: THREE.Vector3, point1: THREE.Vector3) {
    const x = point2.x - point1.x
    const z = point2.z - point1.z
    const p1 = new THREE.Vector2(-z, x)

    p1.normalize()
    p1.multiplyScalar(0.13) // fat
    return p1
  }

  createFenceByPointsXZ({ pointsXZ, y }: { pointsXZ: pointsTypeXZ[]; y: number }) {
    if (!pointsXZ.length || !y) {
      return
    }
    const vertices: number[] = []
    const verticesHead: number[] = []

    let crossP2: THREE.Vector3 = new THREE.Vector3()
    let crossP3: THREE.Vector3 = new THREE.Vector3()
    let moreThen2 = false

    for (let i = 0; i < pointsXZ.length - 1; i++) {
      const point1 = new THREE.Vector3(pointsXZ[i].x, 0, pointsXZ[i].z)
      const point2 = new THREE.Vector3(pointsXZ[i + 1].x, y, pointsXZ[i + 1].z)

      this.drawPlainVertices({ point1, point2, vertices })

      const p1 = this.findPerpendic(point2, point1)

      let pointYP1 = new THREE.Vector3(point1.x - p1.x, y, point1.z - p1.y)
      const pointYP2 = new THREE.Vector3(point2.x + p1.x, y, point2.z + p1.y)
      const pointYP3 = new THREE.Vector3(point2.x - p1.x, y, point2.z - p1.y)
      let pointYP4 = new THREE.Vector3(point1.x + p1.x, y, point1.z + p1.y)

      if (pointsXZ[i + 2]) {
        const point3 = new THREE.Vector3(pointsXZ[i + 2].x, y, pointsXZ[i + 2].z)

        const p2 = this.findPerpendic(point3, point2)

        const pointYP1_2 = new THREE.Vector3(point2.x - p2.x, y, point2.z - p2.y)
        const pointYP2_2 = new THREE.Vector3(point3.x + p2.x, y, point3.z + p2.y)
        const pointYP3_2 = new THREE.Vector3(point3.x - p2.x, y, point3.z - p2.y)
        const pointYP4_2 = new THREE.Vector3(point2.x + p2.x, y, point2.z + p2.y)

        const ipP2_P4 = this.findIntersectionPoint({
          p1x1: pointYP2.x,
          p1z1: pointYP2.z,
          p1x2: pointYP4.x,
          p1z2: pointYP4.z,
          p2x1: pointYP2_2.x,
          p2z1: pointYP2_2.z,
          p2x2: pointYP4_2.x,
          p2z2: pointYP4_2.z,
        })
        const ipP3_P1 = this.findIntersectionPoint({
          p1x1: pointYP3.x,
          p1z1: pointYP3.z,
          p1x2: pointYP1.x,
          p1z2: pointYP1.z,
          p2x1: pointYP3_2.x,
          p2z1: pointYP3_2.z,
          p2x2: pointYP1_2.x,
          p2z2: pointYP1_2.z,
        })

        if (moreThen2) {
          pointYP4 = crossP2.clone()
          pointYP1 = crossP3.clone()
        }
        if (ipP2_P4 && ipP3_P1) {
          moreThen2 = true
          crossP2 = new THREE.Vector3(ipP2_P4.x, y, ipP2_P4.z)
          crossP3 = new THREE.Vector3(ipP3_P1.x, y, ipP3_P1.z)
        }
        this.drawPlainVerticesEdge1({
          point1: pointYP1,
          point2: crossP2, //pointYP2,
          point3: crossP3, //pointYP3,
          point4: pointYP4,
          verticesHead,
        })
      } else {
        if (moreThen2) {
          pointYP4 = crossP2.clone()
          pointYP1 = crossP3.clone()
        }
        this.drawPlainVerticesEdge1({
          point1: pointYP1,
          point2: pointYP2,
          point3: pointYP3,
          point4: pointYP4,
          verticesHead,
        })
      }
    }

    const meshFence = this.createFenceSegment(vertices)

    const meshFenceHead = this.createFenceSegmentHead(verticesHead)
    return { meshFence, meshFenceHead }
  }
  createLineByPointsXZ({ pointsXZ, y }: { pointsXZ: pointsTypeXZ[]; y: number }) {
    if (!pointsXZ.length || !y) {
      return
    }
    const vertices: number[] = []
    const colors: number[] = []
    const color = new THREE.Color()

    for (let i = 0; i < pointsXZ.length; i++) {
      const point1 = new THREE.Vector3(pointsXZ[i].x, 0, pointsXZ[i].z)
      vertices.push(point1.x, point1.y, point1.z)
    }
    const meshFence = this.createFenceLine(vertices, colors)

    return meshFence
  }

  createFenceLine(linePosition: number[], colorsLine: number[]) {
    const positions = []
    const colors = []

    const points = GeometryUtils.hilbert3D(new THREE.Vector3(0, 0, 0), 20.0, 1, 0, 1, 2, 3, 4, 5, 6, 7)

    const spline = new THREE.CatmullRomCurve3(points)
    const divisions = Math.round(12 * points.length)
    const point = new THREE.Vector3()
    const color = new THREE.Color()

    for (let i = 0, l = divisions; i < l; i++) {
      const t = i / l

      spline.getPoint(t, point)
      positions.push(point.x, point.y, point.z)

      color.setHSL(t, 1.0, 0.5, THREE.SRGBColorSpace)
      colors.push(color.r, color.g, color.b)
    }

    const geometry = new LineGeometry()
    geometry.setPositions(linePosition)

    geometry.setColors(colors)

    const matLine = new LineMaterial({
      color: 0xffffff,
      linewidth: 0.005, // in world units with size attenuation, pixels otherwise
      vertexColors: true,
      //resolution:  // to be set by renderer, eventually
      dashed: false,
      alphaToCoverage: true,
    })

    const line = new Line2(geometry, matLine)
    line.computeLineDistances()
    line.scale.set(1, 1, 1)
    //scene.add( line );

    this.meshLine = line

    return line
  }

  createFenceSegment(segmentPositions: number[]) {
    const geometry = new THREE.BufferGeometry()

    geometry.setAttribute('position', new THREE.BufferAttribute(new Float32Array(segmentPositions), 3))

    var quad_uvs = [0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0] // норм!
    var uvs = new Float32Array(quad_uvs)

    geometry.scale(1, 1, this.height)
    geometry.computeVertexNormals()

    const inputTexture = new THREE.TextureLoader().load('wood1.jpg')

    inputTexture.magFilter = THREE.LinearFilter
    inputTexture.colorSpace = THREE.SRGBColorSpace
    inputTexture.generateMipmaps = true
    inputTexture.minFilter = THREE.LinearMipmapLinearFilter

    const material = new THREE.MeshBasicMaterial({ color: 0xffffff, map: inputTexture })

    const meshFence = new THREE.Mesh(geometry, material)

    this.setMeshParams(meshFence)

    this.meshFence = meshFence

    return meshFence
  }
  createFenceSegmentHead(segmentPositions: number[]) {
    const geometry = new THREE.BufferGeometry()
    geometry.setAttribute('position', new THREE.BufferAttribute(new Float32Array(segmentPositions), 3))
    geometry.scale(1, 1, this.height)
    geometry.computeVertexNormals()

    const material = new THREE.MeshPhongMaterial({
      side: THREE.DoubleSide,
      color: this.color,
      transparent: true,
      opacity: 0.7,
      //roughness: 0.212,
      specular: 0.596,
      wireframe: false,
    })

    const meshFence = new THREE.Mesh(geometry, material)

    this.setMeshParams(meshFence)

    this.meshFenceHead = meshFence

    return meshFence
  }

  setMeshParams(meshFence: Mesh | Points) {
    meshFence.updateMatrix()
    meshFence.matrixAutoUpdate = true
    if (this.isCreateFence) {
      meshFence.rotation.x = -Math.PI / 2
    }
  }
  setHeightByCapacity(capacity: number) {
    let hLevel = capacity / this.capacityMax
    if (hLevel > 1) hLevel = 1
    const minHeight = 5
    const maxHeight = 10
    const h = hLevel * minHeight

    this.height = h
    if (this.isCreateFence) {
      this.meshFence && this.meshFence.scale.set(1, 1, h)
      this.meshFenceHead && this.meshFenceHead.scale.set(1, 1, h)
    } else {
      this.meshFence && this.meshFence.scale.set(1, h, 1)
      this.meshFenceHead && this.meshFenceHead.scale.set(1, h, 1)

      let realHeight = 1
      if (this.meshFence) {
        const bbox = new THREE.Box3().setFromObject(this.meshFence)
        // Рассчитываем высоту объекта (разность высоты между верхней и нижней точками объекта)
        realHeight = bbox.max.y - bbox.min.y
      }

      if (this.meshLine && this.meshLine.visible) {
        this.meshLine.position.y = realHeight
      }
    }

    return h
  }
  setColorBySpeed(speed: number, lineMaterials: lineType) {
    let colorLevel = speed / this.speedMax

    let color = 0x08641e
    let matLine = lineMaterials.line1
    if (colorLevel >= 0.9) {
      color = 0x721e1e
      matLine = lineMaterials.line2
    } else if (colorLevel >= 0.7) {
      color = 0xf14646
      matLine = lineMaterials.line3
    } else if (colorLevel >= 0.6) {
      color = 0xf5b719
      matLine = lineMaterials.line4
    } else if (colorLevel >= 0.5) {
      color = 0xfee400
      matLine = lineMaterials.line5
    } else if (colorLevel >= 0.3) {
      color = 0x2cf65c
      matLine = lineMaterials.line6
    }

    if (this.meshFence) {
      const materialC = this.meshFence.material as THREE.ShaderMaterial | THREE.MeshPhongMaterial
      if ('color' in materialC) {
        materialC.color = new THREE.Color(color)
      } else {
      }
    }
    if (this.meshFenceHead) {
      const materialC = this.meshFenceHead.material as THREE.ShaderMaterial | THREE.MeshPhongMaterial
      if ('color' in materialC) {
        materialC.transparent = false
        materialC.color = new THREE.Color(color)
      } else {
        //console.log('no color?')
      }
    }
    if (this.meshFence && this.meshLine) {
      const materialC = this.meshFence.material as THREE.ShaderMaterial | THREE.MeshPhongMaterial
      let materialCLine = this.meshLine.material as LineMaterial
      if ('color' in materialC) {
        this.meshLine.material = matLine
      } else {
        //console.log('no color?')
      }
    }

    return color
  }
}
