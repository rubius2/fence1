import * as THREE from 'three'
import { Mesh, Points, MeshPhongMaterial, ShaderMaterial } from 'three'
import { Scene, PerspectiveCamera, WebGLRenderer } from 'three'

import { Line2 } from './lines/Line2.js'
import { LineMaterial } from './lines/LineMaterial.js'
import { LineGeometry } from './lines/LineGeometry.js'
import { LineSegments2 } from './lines/LineSegments2'
import { LineSegmentsGeometry } from './lines/LineSegmentsGeometry'

import * as GeometryUtils from 'three/examples/jsm/utils/GeometryUtils.js'
import * as BufferGeometryUtils from 'three/examples/jsm/utils/BufferGeometryUtils.js'

import { lineType } from './fence1'

const setMeshParams = (mesh: Mesh | Points) => {
  mesh.scale.set(1.5, 1.5, 7.5)
  mesh.updateMatrix()
  mesh.matrixAutoUpdate = true
}
type fenceParamsPos = {
  position: number[]
  height: number
  color: number
  material: MeshPhongMaterial | ShaderMaterial
}
type fenceParams = {
  segmentPositions: number[]
  height: number
  color: number
  material: MeshPhongMaterial | ShaderMaterial
}

const getMeshFencePos = ({ position, height, color, material }: fenceParamsPos) => {
  const segmentPositions: number[] = []

  for (let i = 0; i < position.length - 4; i++) {
    const offset = i * 2
    const pos = position[i]

    segmentPositions.push(position[0 + offset], position[1 + offset], 0)
    segmentPositions.push(position[2 + offset], position[3 + offset], 0)
    segmentPositions.push(position[2 + offset], position[3 + offset], 1)
    segmentPositions.push(position[2 + offset], position[3 + offset], 1)
    segmentPositions.push(position[0 + offset], position[1 + offset], 1)
    segmentPositions.push(position[0 + offset], position[1 + offset], 0)
  }
  console.log(segmentPositions)

  const meshFence = getmeshFence({ segmentPositions, height, color, material })
  return meshFence
}
const getmeshFence = ({ segmentPositions, height, color, material }: fenceParams) => {
  const geometry = new THREE.BufferGeometry()
  geometry.setAttribute('position', new THREE.BufferAttribute(new Float32Array(segmentPositions), 3))
  geometry.scale(1, 1, height)
  geometry.computeVertexNormals()
  let pos = geometry.attributes.position

  const colors = []
  for (let i = 0; i < segmentPositions.length / 3; i++) {
    colors.push(Math.random(), Math.random(), Math.random())
  }
  geometry.setAttribute('color', new THREE.BufferAttribute(new Float32Array(colors), 3))

  // @ts-ignore
  material.color = new THREE.Color(color)

  const meshFence = new THREE.Mesh(geometry, material)
  const materialC = meshFence.material as THREE.ShaderMaterial | THREE.MeshPhongMaterial

  if ('color' in materialC) {
    console.log(materialC.color)
  } else {
    console.log('no color?')
  }

  setMeshParams(meshFence)
  meshFence.rotation.x = -Math.PI / 2

  return meshFence
}

const simpleLineSegments = ({ scene }: { scene: Scene }) => {
  // line material
  var material = new THREE.LineBasicMaterial({
    color: 0xffffff,
  })

  const indices = [0, 1, 1, 2, 2, 3, 4, 5]

  const points = []
  const colors = []

  points.push(-10, 0, 0)
  points.push(0, 10, 0)
  points.push(10, 0, 0)
  points.push(20, 10, 0)

  for (let i = 0; i < points.length / 3; i++) {
    colors.push(0, 1, 0)
  }

  var geometry = new LineGeometry()

  geometry.setPositions(points)
  geometry.setColors(colors)
  geometry.setIndex(indices)

  // @ts-ignore
  var line = new THREE.LineSegments(geometry, material)
  scene.add(line)
}

const getLine2 = ({ scene }: { scene: Scene }) => {
  const geometries = []
  const k = 10

  for (let i = 0; i < 10; i++) {
    const r = (Math.random() * 0xffffffff) | 0
    const g = (Math.random() * 0xffffffff) | 0
    const b = (Math.random() * 0xffffffff) | 0

    const points: number[] = []
    const colors: number[] = []

    points.push(-10, 0, 0 + i * k)
    points.push(0, 10, 0 + i * k)
    points.push(10, 0, 0 + i * k)
    points.push(20, 10, 0 + i * k)

    for (let i = 0; i < points.length / 3; i++) {
      colors.push(r, g, b)
    }

    const geometry = new LineGeometry()
    geometry.setPositions(points)
    geometry.setColors(colors)

    const matLine = new LineMaterial({
      color: 0xffffff,
      linewidth: 0.005, // in world units with size attenuation, pixels otherwise
      vertexColors: true,
      //resolution:  // to be set by renderer, eventually
      dashed: false,
      alphaToCoverage: true,
    })

    const line = new Line2(geometry, matLine)

    line.computeLineDistances()
    line.scale.set(1, 1, 1)
    // geometries.push(line)
    geometries.push(geometry)
    /*** */
    // scene.add(line)
  }
  const g1 = new THREE.BufferGeometry()
  const points = [0, 2, 2, 2, 2, 0, 0, 2, -2, -2, 2, 0]

  g1.setAttribute('position', new THREE.BufferAttribute(new Float32Array(points), 3))
  g1.setIndex([0, 1, 2, 0, 2, 3])
  g1.computeVertexNormals()

  /** */
  const g2 = new THREE.BufferGeometry()
  const points2 = [0, -2, 2, 2, -2, 0, 0, -2, -2, -2, -2, 0]

  g2.setAttribute('position', new THREE.BufferAttribute(new Float32Array(points2), 3))
  g2.setIndex([0, 1, 2, 0, 2, 3])
  g2.computeVertexNormals()

  const g3 = new THREE.BufferGeometry()
  const points3 = [4, -2, 3, 5, -1, 9, 9, -3, -7, 2, -8, 0]

  g3.setAttribute('position', new THREE.BufferAttribute(new Float32Array(points3), 3))
  g3.setIndex([0, 2, 3])
  g3.computeVertexNormals()
  /** */

  const merge = BufferGeometryUtils.mergeGeometries([g1, g2, g3], false)

  const line = new THREE.Line(merge, new THREE.LineBasicMaterial({ color: 'white', linewidth: 2 }))

  scene.add(line)
}

export {
  getMeshFencePos,
  getmeshFence,
  setMeshParams,
  getLine2,
  simpleLineSegments,
}
